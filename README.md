#### A file-stored LIFO queue

    Usage (any of):
               fque push ITEM - append an item
               fque pop       - print out and remove the last item
               fque list      - print out all items
               fque dump      - print out and remove all items
       LINES | fque fill      - append items from STDIN
    Environment vars:
       FQUE - path to the queue-file (default: @fque)
