use std::io;

pub enum AppError {
	MyError(String),
	IoError(io::Error),
}

impl From<io::Error> for AppError {
	fn from(error: io::Error) -> Self {
		Self::IoError(error)
	}
}

impl From<&'static str> for AppError {
	fn from(error: &'static str) -> Self {
		Self::MyError(error.to_string())
	}
}

impl From<String> for AppError {
	fn from(error: String) -> Self {
		Self::MyError(error)
	}
}
