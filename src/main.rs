use std::{
	ffi::OsString,
	fs::{File, OpenOptions},
	io::{self, BufReader, Seek, SeekFrom, Write},
	os::unix::ffi::OsStrExt,
	path::{Path, PathBuf},
};

use flopen::OpenAndLock;

mod io_ext;
use io_ext::BufReadExt;
mod app_err;
use app_err::AppError;

struct App;
impl App {
	const HELP: &str = "\
About: a file-stored LIFO queue
Usage (any of):
           fque push ITEM - append an item
           fque pop       - print out and remove the last item
           fque list      - print out all items
           fque dump      - print out and remove all items
   LINES | fque fill      - append items from STDIN
Environment vars:
   FQUE - path to the queue-file (default: @fque)";
	fn die(self) -> ! {
		std::process::exit(1)
	}
	fn help(self) -> App {
		eprintln!("{}", Self::HELP);
		self
	}
	fn err(self, msg: impl std::fmt::Display) -> App {
		eprintln!("[fque] ERROR: {msg}\n");
		self
	}
}

fn writeln(writer: &mut impl Write, line: &[u8]) -> io::Result<()> {
	writer.write_all(line)?;
	writer.write_all(b"\n")?;
	Ok(())
}

fn abort_on_empty_que(que: &Path) {
	if !que.is_file() {
		App.die();
	}
}

fn push(mut qf: File, line: &[u8]) -> io::Result<()> {
	qf.seek(SeekFrom::End(0))?;
	writeln(&mut qf, line)?;
	Ok(())
}

fn pop_last_item(que: &Path, qf: File) -> io::Result<Option<Vec<u8>>> {
	let lines = BufReader::new(&qf).bytelines().map_while(Result::ok);
	if let Some(last_item) = lines.last() {
		let qfile_len = que.metadata()?.len();
		let new_length = qfile_len - last_item.len() as u64 - 1;
		if new_length == 0 {
			drop(qf);
			std::fs::remove_file(que)?;
		} else {
			qf.set_len(new_length)?;
		}
		return Ok(Some(last_item));
	}
	Ok(None)
}

fn open_que(que: &Path) -> io::Result<File> {
	OpenOptions::new()
		.create(true)
		.write(true)
		.read(true)
		.open_and_lock(que)
}

fn run(
	mut args: impl Iterator<Item = OsString>,
	que: &Path,
	writer: &mut impl Write,
) -> Result<(), AppError> {
	match args.next() {
		Some(cmd) if cmd == *"push" => match args.next() {
			Some(item) => push(open_que(que)?, item.as_bytes())?,
			None => return Err("no ITEM to push".into()),
		},
		Some(cmd) if cmd == *"fill" => {
			let mut qf = open_que(que)?;
			qf.seek(SeekFrom::End(0))?;
			io::copy(&mut io::stdin().lock(), &mut qf)?;
		}
		Some(cmd) if cmd == *"pop" => {
			abort_on_empty_que(que);
			match pop_last_item(que, open_que(que)?) {
				Ok(Some(item)) => writeln(writer, &item)?,
				Ok(None) => return Err("nothing to pop".into()),
				Err(e) => return Err(e.into()),
			}
		}
		Some(cmd) if cmd == *"dump" => {
			abort_on_empty_que(que);
			let mut qf = open_que(que)?;
			io::copy(&mut qf, writer)?;
			drop(qf);
			std::fs::remove_file(que)?;
		}
		Some(cmd) if cmd == *"list" => {
			abort_on_empty_que(que);
			io::copy(&mut open_que(que)?, writer)?;
		}
		Some(other) => return Err(format!("invalid command: {}", other.to_string_lossy()).into()),
		None => return Err("no command provided".into()),
	}
	Ok(())
}

fn main() {
	let que = std::env::var_os("FQUE").map_or(PathBuf::from("@fque"), PathBuf::from);
	let mut writer = io::stdout().lock();
	let args = std::env::args_os().skip(1);
	run(args, &que, &mut writer).unwrap_or_else(|err| match err {
		AppError::IoError(e) => App.err(e).die(),
		AppError::MyError(e) => App.err(e).help().die(),
	});
}
