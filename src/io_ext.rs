use std::io::{self, BufRead};

pub struct BLines<B> {
	buf: Vec<u8>,
	reader: B,
}
pub trait BufReadExt: BufRead {
	fn bytelines(self) -> BLines<Self>
	where Self: Sized {
		BLines { buf: Vec::new(), reader: self }
	}
}
impl<B: BufRead> Iterator for BLines<B> {
	type Item = io::Result<Vec<u8>>;

	fn next(&mut self) -> Option<Self::Item> {
		self.buf.clear();
		match self.reader.read_until(b'\n', &mut self.buf) {
			Err(e) => Some(Err(e)),
			Ok(0) => None,
			Ok(n) => {
				if self.buf[n - 1] == b'\n' {
					self.buf.pop().unwrap();
				}
				Some(Ok(self.buf.clone()))
			}
		}
	}
}
impl<B: BufRead> BufReadExt for B {}
